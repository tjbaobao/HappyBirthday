package com.tjbaobao.happybirthday;

/**
 * Created by TJbaobao on 2017/12/2.
 */

public class AppEnum {

    public enum MeMenuEnum
    {
        Brighter(R.drawable.anim_edit_px_brighter,"光感启动");

        public int imageId ;
        public String name ;

        MeMenuEnum(int imageId, String name) {
            this.imageId = imageId;
            this.name = name;
        }
    }
}
