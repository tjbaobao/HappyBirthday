package com.tjbaobao.happybirthday;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.tjbaobao.videoplayer.view.base.BaseProgressView;


/**
 * 自定义播放器进度条
 * @author TJbaobao
 *
 */
public class VideoProgressBar extends BaseProgressView {
	private float progress=0;
	private int backgroundColor= Color.parseColor("#eb3953") ;//默认背景颜色
	private int progressColor = Color.parseColor("#eb3953");//默认进度条颜色
	private int progressBGColor = Color.parseColor("#c9c9c9");//默认进度条背景颜色
	private int textColor = Color.parseColor("#ffffff");//默认进度条背景颜色
	private boolean bStart ;
	private String text = "";
	private int textSize = 20;
	private int viewWidth,viewHeight;
	private boolean bFinish = false;

	
	public VideoProgressBar(Context context) {
		super(context);
	}
	public VideoProgressBar(Context context, AttributeSet attrs)
	{
		super(context ,attrs);
	}
	public VideoProgressBar(Context context, AttributeSet attrs, int defStyle)
	{
		super(context ,attrs,defStyle);
	}

	@Override
	public void setProgress(float progress) {
		if(!bStart)
		{
			bStart = true;
		}
		this.progress = progress ;
		postInvalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Paint paint = new Paint();
		paint.setAntiAlias(true);// 消除锯齿效果
		if(bStart)
		{
			//启动进度条
			//canvas.drawColor(progressBGColor);
			paint.setStyle(Paint.Style.FILL);
			paint.setColor(progressBGColor);
	        canvas.drawRect(0, (viewHeight-viewHeight/4)/2, viewWidth,(viewHeight-viewHeight/4)/2+viewHeight/4, paint);
	        paint.setColor(progressColor);
			float width = progress*viewWidth;
	        canvas.drawRect(0, (viewHeight-viewHeight/4)/2, width-2,(viewHeight-viewHeight/4)/2+viewHeight/4, paint);
	        
	        canvas.drawCircle(width-viewHeight/2,viewHeight/2, viewHeight/2-2, paint);
	        paint.setStyle(Paint.Style.STROKE);
	        paint.setColor(Color.WHITE);
	        canvas.drawCircle(width-viewHeight/2,viewHeight/2, viewHeight/2-2, paint);
		}
		else
		{
			//未启动进度条
			paint.setColor(backgroundColor);
			canvas.drawRect(0, (viewHeight-viewHeight/3)/2, viewWidth,(viewHeight-viewHeight/3)/2+viewHeight/3, paint);
			paint.setColor(Color.WHITE);
			drawTextCenter(canvas,text);
		}
		
	}

	public void setError(String text)
	{
		bStart = false;
		this.text = text;
		postInvalidateDelayed(1);
	}
	
	public void finish()
	{
		bFinish = true;
		postInvalidateDelayed(1);
	}
	private void drawTextCenter(Canvas canvas, String text)
	{
		Paint mPaint = new Paint();
		mPaint.setStrokeWidth(3);
		mPaint.setTextSize(textSize);
		mPaint.setColor(textColor);
		mPaint.setTextAlign(Align.LEFT);
		Rect bounds = new Rect();
		mPaint.getTextBounds(text, 0, text.length(), bounds);
		FontMetricsInt fontMetrics = mPaint.getFontMetricsInt();
		int baseline = (getMeasuredHeight() - fontMetrics.bottom + fontMetrics.top) / 2 - fontMetrics.top;
		canvas.drawText(text, getMeasuredWidth() / 2 - bounds.width() / 2, baseline, mPaint);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(event.getAction()== MotionEvent.ACTION_MOVE||event.getAction()== MotionEvent.ACTION_DOWN)
		{
			if(event.getAction()== MotionEvent.ACTION_DOWN)
			{
				if(onProgressChangeListener!=null)
				{
					onProgressChangeListener.onStartTrackingTouch();
				}
			}
			progress= event.getX()/(float)viewWidth;
			if(onProgressChangeListener!=null)
			{
				onProgressChangeListener.onProgressChanged(progress);
			}
			invalidate();
			return true;
		}
		if(event.getAction()== MotionEvent.ACTION_UP)
		{
			if(onProgressChangeListener!=null)
			{
				onProgressChangeListener.onStopTrackingTouch();
			}
		}
		return false;
	}

	
	private int getColorForRes(int resID)
	{
		return context.getResources().getColor(resID);
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		this.viewWidth = measureWidth(widthMeasureSpec);  
        this.viewHeight = measureHeight(heightMeasureSpec);  
        setMeasuredDimension(viewWidth, viewHeight);
	}

	/**
	 * 计算组件宽度
	 */
	private int measureWidth(int measureSpec) {
		int specMode = View.MeasureSpec.getMode(measureSpec);
		int specSize = View.MeasureSpec.getSize(measureSpec);
		int result = 500;
		if (specMode == View.MeasureSpec.AT_MOST) {
			result = specSize;
		}
		else if (specMode == View.MeasureSpec.EXACTLY) {

			result = specSize;
		}
		return result;
	}

	/**
	 * 计算组件高度
	 */
	private int measureHeight(int measureSpec) {

		int specMode = View.MeasureSpec.getMode(measureSpec);
		int specSize = View.MeasureSpec.getSize(measureSpec);
		int result = 500;
		if (specMode == View.MeasureSpec.AT_MOST) {
			result = specSize;
		} else if (specMode == View.MeasureSpec.EXACTLY) {

			result = specSize;
		}
		return result;
	}
	public void setMBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
		postInvalidateDelayed(1); 
	}
	public int getProgressColor() {
		return progressColor;
	}
	public void setProgressColor(int progressColor) {
		this.progressColor = progressColor;
	}
	public int getProgressBGColor() {
		return progressBGColor;
	}
	public void setProgressBGColor(int progressBGColor) {
		this.progressBGColor = progressBGColor;
	}
	public int getTextColor() {
		return textColor;
	}
	public void setTextColor(int textColor) {
		this.textColor = textColor;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
		postInvalidateDelayed(1); 
	}
	public int getTextSize() {
		return textSize;
	}
	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}
	
}
