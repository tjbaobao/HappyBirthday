package com.tjbaobao.happybirthday;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.tjbaobao.framework.base.BaseActivity;
import com.tjbaobao.framework.entity.ui.TitleBarInfo;
import com.tjbaobao.framework.ui.BaseTitleBar;
import com.tjbaobao.framework.util.Tools;

/**
 * Created by TJbaobao on 2017/11/15.
 */

public abstract class AppActivity extends BaseActivity implements BaseTitleBar.OnTitleBarClickListener{

    private BaseTitleBar titleBar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        immersiveStatusBar();
        initValues(savedInstanceState);
        initView();
        initTitleBar();
        onLoadData();
    }

    /**
     * 初始化值
     */
    private void initValues(Bundle savedInstanceState)
    {
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
        onInitValues(savedInstanceState);
    }
    /**
     * 初始化组件与布局
     */
    private void initView()
    {
        onInitView();
    }

    private void initTitleBar()
    {
        titleBar = this.findViewById(R.id.titleBar);
        if(titleBar!=null)
        {
            titleBar.setOnTitleBarClickListener(this);
            titleBar.setBackgroundColor(getColorById(R.color.app_theme_color));
            titleBar.addImageToRight(R.drawable.ic_setting);
            onInitTitleBar(titleBar);
        }
    }

    protected abstract void onInitValues(Bundle savedInstanceState);

    protected abstract void onInitView();

    protected abstract void onInitTitleBar(BaseTitleBar titleBar);

    protected abstract void onLoadData();

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    @Override
    public <V extends TitleBarInfo.BaseView> void onTitleBarClick(int i, int i1, V v) {
        if(i== BaseTitleBar.LAYOUT_LEFT)
        {
           if(i1==0)
           {
               Tools.showToast("点击了返回键");
           }
        }
    }

    // ***************双击退出应用开始*************
    private long startTime = 0;
    private boolean isOnclickTwoExit = false;
    private int exitTime = 2000;
    /**
     * 双击两次退出应用
     */
    public void isOnclickTwoExit()
    {
        setOnclickTwoExit(exitTime);
    }
    public void isOnclickTwoExit(boolean isExit)
    {
        isOnclickTwoExit = isExit;
        if(isExit)
        {
            setOnclickTwoExit(exitTime);
        }
    }
    public void setOnclickTwoExit(int time)
    {
        isOnclickTwoExit = true ;
        exitTime = time;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK&&isOnclickTwoExit)
        {
            long nowTime = System.currentTimeMillis();
            if (nowTime - startTime < exitTime)
            {
                this.finish();
            }
            else
            {
                startTime = System.currentTimeMillis();
                return onClickTwoExit();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onClickTwoExit()
    {
        Tools.showToast("再次点击退出程序");
        return true;
    }
    // ***************双击退出应用结束*************
}
