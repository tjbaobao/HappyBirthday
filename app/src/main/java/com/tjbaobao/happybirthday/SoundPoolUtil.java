package com.tjbaobao.happybirthday;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Build;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class SoundPoolUtil implements OnLoadCompleteListener {
	
	private Context context ;
	private SoundPool soundPool;
	private int priority = 0;
	private float rate = 1.0f ;
	private boolean isLoadThenPlay  = false;
	private Map<Integer,Integer> hashMap = new HashMap<Integer, Integer>();
	private Map<Integer,Float> hasVolume = new HashMap<Integer,Float>();
	@SuppressLint("NewApi")
	public SoundPoolUtil(Context context) {
		this.context = context;
		if(Build.VERSION.SDK_INT>=21){
			SoundPool.Builder spb = new SoundPool.Builder();
			spb.setMaxStreams(3);
			//spb.setAudioAttributes(null);    //转换音频格式
			soundPool = spb.build();      //创建SoundPool对象
		}
		else
		{
			soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC,5);
		}
		soundPool.setOnLoadCompleteListener(this);
	}
	public void load(int[] resIds)
	{
		int i=0;
		for(int resId:resIds)
		{
			hashMap.put(i,soundPool.load(context, resId, priority));
			i++;
		}
	}
	public void load(FileDescriptor fileDescriptor, int position)
	{
		int afd = soundPool.load(fileDescriptor, 69 * position, 69, 1);
	}
	public void load(int resId)
	{
		int afd = soundPool.load(context, resId, priority);
	}
	public void load(float volume,int resId)
	{
		int afd = soundPool.load(context, resId, priority);
		hasVolume.put(afd,volume);
	}
	public void load(float volume,String name)
	{
		AssetManager am = context.getAssets();
		try {
			AssetFileDescriptor assetFileDescriptor = am.openFd(name);
			int afd = soundPool.load(assetFileDescriptor,priority);
			hasVolume.put(afd,volume);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void loadThenPlay(String path)
	{
		isLoadThenPlay = true ;
		soundPool.stop(0);
		hashMap.put(0,soundPool.load(path, priority));
	}
	public void play(int i)
	{
		if(hashMap.containsKey(i))
		{
			soundPool.resume(soundPool.play(hashMap.get(i), 1f, 1f, 0, 0, 1f));
		}
	}
	public void play()
	{
		play(0);
	}
	public void release()
	{
		soundPool.release();
	}
	@Override
	public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        float volume = 1;
		if(hasVolume.containsKey(sampleId))
        {
            volume = hasVolume.get(sampleId);
            if(volume==0)
            {
                volume = 1;
            }
        }
		soundPool.setVolume(soundPool.play(sampleId, 1f, 1f, 0, 0, rate),volume,volume);
		if(isLoadThenPlay)
		{
			//soundPool.resume(soundPool.play(sampleId, 1f, 1f, 0, 0, rate));
		}
	}
	public float getRate() {
		return rate;
	}
	public void setRate(float rate) {
		this.rate = rate;
	}

	public boolean isLoadThenPlay() {
		return isLoadThenPlay;
	}

	public void setLoadThenPlay(boolean loadThenPlay) {
		isLoadThenPlay = loadThenPlay;
	}
}
