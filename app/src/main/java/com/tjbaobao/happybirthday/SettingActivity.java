package com.tjbaobao.happybirthday;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.tjbaobao.framework.entity.ui.TitleBarInfo;
import com.tjbaobao.framework.ui.BaseTitleBar;
import com.tjbaobao.framework.util.Tools;
import com.tjbaobao.videoplayer.listener.OnProgressChangeListener;

/**
 * Created by TJbaobao on 2018/1/9.
 */

public class SettingActivity extends AppActivity implements BaseTitleBar.OnTitleBarClickListener, CompoundButton.OnCheckedChangeListener,OnProgressChangeListener {

    private Switch mSwitch ;
    private VideoProgressBar mProgressBar ;
    private TextView tv_value;
    @Override
    protected void onInitValues(Bundle savedInstanceState) {

    }

    @Override
    protected void onInitView() {
        setContentView(R.layout.setting_activity_layout);
        mSwitch = this.findViewById(R.id.switchView);
        mSwitch.setOnCheckedChangeListener(this);
        mSwitch.setChecked((Boolean) Tools.getSharedPreferencesValue("switch",false));
        mProgressBar = this.findViewById(R.id.progressBar);
        tv_value = this.findViewById(R.id.tv_value);
        mProgressBar.setProgress(((float)(Integer)Tools.getSharedPreferencesValue("value",8))/16f);
        mProgressBar.setOnProgressChangeListener(this);
        tv_value.setText(Tools.getSharedPreferencesValue("value",8)+"");
    }

    @Override
    protected void onInitTitleBar(BaseTitleBar titleBar) {
        titleBar.removeView(BaseTitleBar.LAYOUT_RIGHT,0);
        titleBar.setTitle("设置");
        titleBar.addImageToLeft(R.drawable.fw_ic_back);
    }

    @Override
    protected void onLoadData() {

    }

    @Override
    public <V extends TitleBarInfo.BaseView> void onTitleBarClick(int i, int i1, V v) {
        if(i==BaseTitleBar.LAYOUT_LEFT&&i1==0)
        {
            this.finish();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        Tools.setSharedPreferencesValue("switch",b);
    }


    @Override
    public void onProgressChanged(float v) {
        tv_value.setText((int)(16f*v)+"");
        Tools.setSharedPreferencesValue("value",(int)(16f*v));
    }

    @Override
    public void onStartTrackingTouch() {

    }

    @Override
    public void onStopTrackingTouch() {

    }
}
